import React from 'react'
import DrumPad from './DrumPad.js'

class PadBank extends React.Component{
    // eslint-disable-next-line no-useless-constructor
    constructor(props) {
        super(props);
    }

    render() {

        let padBank;
        this.props.checkedPower ?
            padBank = this.props.currentPadBank.map(( drumObj, i, padBankArr ) => {
                return (
                    <DrumPad key={`${i}header`} clipId={padBankArr[i].id} clip={padBankArr[i].url}
                             keyTrigger={padBankArr[i].keyTrigger} keyCode={padBankArr[i].keyCode}
                             updateDisplay={this.props.updateDisplay} checkedPower={this.props.checkedPower}/>
                )

            }) :
            padBank = this.props.currentPadBank.map(( drumObj, i, padBankArr ) => {
                return (
                    <DrumPad key={`${i}piano`} clipId={padBankArr[i].id} clip={padBankArr[i].url}
                             keyTrigger={padBankArr[i].keyTrigger}
                             keyCode={padBankArr[i].keyCode} updateDisplay={this.props.updateDisplay}
                             checkedPower={this.props.checkedPower}/>
                )
            });

        return (
            <div className="px-4 col-12 col-sm-6 col-lg-6 mb-2 ml-lg-4">
                {padBank}
            </div>

        )
    }
}

export default PadBank;