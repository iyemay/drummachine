import React from 'react'


class DrumPad extends React.Component {
    constructor(props) {
        super(props);

        this.playSound = this.playSound.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    componentDidMount() {
        document.addEventListener('keydown', this.handleKeyPress);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.handleKeyPress);
    }

    playSound() {
        const sound = document.getElementById(this.props.keyTrigger);
        sound.currentTime = 0;
        sound.play();
        this.props.updateDisplay(this.props.clipId.replace(/-/g, ' '));
    }

    handleKeyPress(event) {
        if (event.keyCode === this.props.keyCode) {
            this.playSound();
        }
    }

    render() {
        return (
            <div className="col-4 d-inline-block p-0">
                <div className="square-div">
                    <div className="square-container">
                        <div id={this.props.clipId} className="drum-pad py-2 px-3 my-1
                            d-flex align-items-center justify-content-center" onClick={this.playSound}>
                            <audio className='clip' id={this.props.keyTrigger} src={this.props.clip}/>
                            <span className="key-text"> {this.props.keyTrigger} </span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default DrumPad;