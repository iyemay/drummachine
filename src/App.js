import React from 'react';
import './App.scss';
import PadBank from './components/PadBank.js';

const bankOne = [{
    keyCode: 81,
    keyTrigger: 'Q',
    id: 'Heater-1',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-1.mp3'
}, {
    keyCode: 87,
    keyTrigger: 'W',
    id: 'Heater-2',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-2.mp3'
}, {
    keyCode: 69,
    keyTrigger: 'E',
    id: 'Heater-3',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-3.mp3'
}, {
    keyCode: 65,
    keyTrigger: 'A',
    id: 'Heater-4_1',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-4_1.mp3'
}, {
    keyCode: 83,
    keyTrigger: 'S',
    id: 'Heater-6',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-6.mp3'
}, {
    keyCode: 68,
    keyTrigger: 'D',
    id: 'Dsc_Oh',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Dsc_Oh.mp3'
}, {
    keyCode: 90,
    keyTrigger: 'Z',
    id: 'Kick_n_Hat',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Kick_n_Hat.mp3'
}, {
    keyCode: 88,
    keyTrigger: 'X',
    id: 'RP4_KICK_1',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/RP4_KICK_1.mp3'
}, {
    keyCode: 67,
    keyTrigger: 'C',
    id: 'Cev_H2',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Cev_H2.mp3'
}];

const bankTwo = [{
    keyCode: 81,
    keyTrigger: 'Q',
    id: 'Chord-1',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Chord_1.mp3'
}, {
    keyCode: 87,
    keyTrigger: 'W',
    id: 'Chord-2',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Chord_2.mp3'
}, {
    keyCode: 69,
    keyTrigger: 'E',
    id: 'Chord-3',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Chord_3.mp3'
}, {
    keyCode: 65,
    keyTrigger: 'A',
    id: 'Give_us_a_light',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Give_us_a_light.mp3'
}, {
    keyCode: 83,
    keyTrigger: 'S',
    id: 'Dry_Ohh',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Dry_Ohh.mp3'
}, {
    keyCode: 68,
    keyTrigger: 'D',
    id: 'Bld_H1',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Bld_H1.mp3'
}, {
    keyCode: 90,
    keyTrigger: 'Z',
    id: 'Punchy_kick_1',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/punchy_kick_1.mp3'
}, {
    keyCode: 88,
    keyTrigger: 'X',
    id: 'Side_stick_1',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/side_stick_1.mp3'
}, {
    keyCode: 67,
    keyTrigger: 'C',
    id: 'Brk_Snr',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Brk_Snr.mp3'
}];

class DrumMachine extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            displayText: 'Smooth Piano Kit',
            checkedPower: false,
            currentPadBank: bankTwo
        };

        this.displayClipName = this.displayClipName.bind(this);
        this.handleSwitchChange = this.handleSwitchChange.bind(this);

    }

    displayClipName(name) {
        this.setState({
            displayText: name
        });
    }

    handleSwitchChange = () => {
        if ( this.state.checkedPower ) {
            console.log("This is the if")
            this.setState({
                currentPadBank: bankTwo,
                displayText: 'Smooth Piano Kit',
                checkedPower: false
            })
        } else {
            this.setState({
                currentPadBank: bankOne,
                displayText: 'Header Kit',
                checkedPower: true
            })
        }
    }

    render() {
        return (
            <div id="drum-machine" className="inner-container d-flex flex-column w-100 my-5 mx-5">
                <div className="header-drum px-1 align-items-sm-start mt-lg-3
                                d-flex justify-content-center align-items-center">
                    <p>
                        <strong>
                            React JS Drum Machine
                        </strong>
                    </p>
                </div>

                <div className="d-flex flex-column-reverse flex-sm-row mb-lg-3">

                    <PadBank checkedPower={this.state.checkedPower} updateDisplay={this.displayClipName}
                             currentPadBank={this.state.currentPadBank}/>

                    <div className="py-2 col-12 col-sm-6 col-lg-5 ml-lg-5 mb-2
                                    d-flex flex-column align-items-center align-content-center
                                    justify-content-center">

                        <div className="custom-control custom-switch my-1">
                            <input type="checkbox" className="custom-control-input" id="customSwitches"
                                   onChange={this.handleSwitchChange}/>
                            <label className="custom-control-label" htmlFor="customSwitches">Header Kit</label>
                        </div>

                        <div id="display" className="px-2">
                            <p className="text-white pt-3" onChange={this.displayClipName}>
                                {this.state.displayText}
                            </p>
                        </div>

                    </div>

                </div>

            </div>

        );
    }
}

export default DrumMachine;
